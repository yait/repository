# repository

The yait repository is, obviously the repository that yait is gonna call. It can be remote, like this one or local (configurable with `.yait.toml` file).

## Architecture

### Descriptor

The yait repository must contain at its base a `descriptor.yaml`, it can have another name, but in this case, yait must be configured with `yait config descriptor.name another_name.yaml`.

The descriptor describes for each platforms (OS + ARCH), the list of packages available and the extension of package scripts.

### Packages

In the folder `packages` will be found every packages available, the package folder name must correspond to the name written in the descriptor.

For every platform, a script with the name of the platform and the provided extension must be created.

A script is composed of three functions and a switch case :
- `_install` which is called to install the package
- `_upgrade` which is called to upgrade the package
- `_remove` which is called to remove the package

## What's a package ?

Anything, but a package must be retrieved from a public source and a trusted one. It can be github (official repository), the official website, or the package manager (but in that case, yait shouldn't be required).
