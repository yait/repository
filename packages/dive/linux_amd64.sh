#!/bin/sh

_install_or_upgrade() {
    download_url=$(curl -L "https://api.github.com/repos/wagoodman/dive/releases/latest" | jq '.assets[] | select(.name|match("dive_.*_linux_amd64.deb")) | .browser_download_url' | tr -d \")
    curl -L "${download_url}" -o /tmp/dive.deb
    sudo apt-get install /tmp/dive.deb
}

_install() {
    _install_or_upgrade
}

_upgrade() {
    _install_or_upgrade
}

_remove() {
    sudo apt-get remove dive
}

case $1 in
    _install) _install;;
    _upgrade) _upgrade;;
    _remove) _remove;;
esac