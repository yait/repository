#!/bin/sh

_install_or_upgrade() {
    sudo mkdir -p /usr/local/openapi-generator
    sudo curl -L "https://raw.githubusercontent.com/OpenAPITools/openapi-generator/master/bin/utils/openapi-generator-cli.sh" \
        -o /usr/local/openapi-generator/openapi-generator-cli
    sudo chmod +x /usr/local/openapi-generator/openapi-generator-cli
    sudo ln -sf /usr/local/openapi-generator/openapi-generator-cli /usr/local/bin/openapi-generator-cli
}

_install() {
    _install_or_upgrade
}

_upgrade() {
    _install_or_upgrade
}

_remove() {
    sudo rm -r /usr/local/openapi-generator
    sudo rm /usr/local/bin/openapi-generator-cli-*
    sudo find /usr/local/bin -xtype l -delete
}

case $1 in
    _install) _install;;
    _upgrade) _upgrade;;
    _remove) _remove;;
esac