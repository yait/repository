#!/bin/sh

_install_or_upgrade() {
    download_url=$(curl -L "https://go.dev/dl/?mode=json" | jq '.[].files[].filename | select(.|match(".*.linux-amd64.tar.gz"))' | sort -r | head -n 1 | tr -d \")
    curl -L "https://go.dev/dl/${download_url}" -o /tmp/go.tar.gz
    sudo tar -C /usr/local -xzf /tmp/go.tar.gz --overwrite
    sudo ln -sf /usr/local/go/bin/* /usr/local/bin
}

_install() {
    _install_or_upgrade

    grep -q "export GOROOT=/usr/local/go" /etc/profile || sudo sh -c 'echo "export GOROOT=/usr/local/go" >> /etc/profile'
    # shellcheck disable=SC2016
    grep -q 'export PATH="${PATH}:${HOME}/go/bin"' ~/.bashrc || echo 'export PATH="${PATH}:${HOME}/go/bin"' >> ~/.bashrc
}

_upgrade() {
    _install_or_upgrade
}

_remove() {
    sudo rm -r /usr/local/go
    sudo find /usr/local/bin -xtype l -delete

    # shellcheck disable=SC2016
    sed -i '#export PATH="${PATH}:${HOME}/go/bin"#d' ~/.bashrc
    sudo sed -i '#export GOROOT=/usr/local/go#d' /etc/profile
}

case $1 in
    _install) _install;;
    _upgrade) _upgrade;;
    _remove) _remove;;
esac