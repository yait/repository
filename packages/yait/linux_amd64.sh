#!/bin/sh

_install_or_upgrade() {
    curl -L "https://gitlab.com/api/v4/projects/43559830/releases/permalink/latest/downloads/yait_linux_amd64.deb" -o /tmp/yait.deb
    sudo apt-get install /tmp/yait.deb
}

_install() {
    _install_or_upgrade
}

_upgrade() {
    _install_or_upgrade
}

_remove() {
    sudo apt-get remove yait
}

case $1 in
    _install) _install;;
    _upgrade) _upgrade;;
    _remove) _remove;;
esac