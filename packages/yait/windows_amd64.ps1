function _install_or_upgrade {
    iwr -Uri "https://gitlab.com/api/v4/projects/43559830/releases/permalink/latest/downloads/yait_windows_amd64.zip" -OutFile "$env:temp\yait.zip"
    Expand-Archive -Force -Path "$env:temp\yait.zip" -DestinationPath "$env:userprofile\bin\yait"
    copy -Path "$env:userprofile\bin\yait\yait.exe" -Destination "$env:userprofile\bin\yait.exe"
}

function _install {
    _install_or_upgrade
    if (!("$env:path" -like "$env:userprofile\bin")) {
        [Environment]::SetEnvironmentVariable("Path", "$env:Path;$env:userprofile\bin", "User")
    }
}

function _upgrade {
    _install_or_upgrade
}

function _remove {
    ri "$env:userprofile\bin\yait"
    ri "$env:userprofile\bin\yait.exe"
}

switch ($args[0]) {
    "_install" {
        _install
    }
    "_upgrade" {
        _upgrade
    }
    "_remove" {
        _remove
    }
}