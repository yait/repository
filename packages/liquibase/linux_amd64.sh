#!/bin/sh

_install_or_upgrade() {
    download_url=$(curl -L "https://api.github.com/repos/liquibase/liquibase/releases/latest" | jq '.assets[] | select(.name|match("liquibase-.*.tar.gz")) | .browser_download_url' | tr -d \")
    curl -L "${download_url}" -o /tmp/liquibase.tar.gz
    
    sudo mkdir -p /usr/local/liquibase
    sudo tar -C /usr/local/liquibase -xzf /tmp/liquibase.tar.gz --overwrite
    sudo ln -sf /usr/local/liquibase/liquibase /usr/local/bin/liquibase
}

_install() {
    _install_or_upgrade
}

_upgrade() {
    _install_or_upgrade
}

_remove() {
    sudo rm -r /usr/local/liquibase
    sudo find /usr/local/bin -xtype l -delete
}

case $1 in
    _install) _install;;
    _upgrade) _upgrade;;
    _remove) _remove;;
esac