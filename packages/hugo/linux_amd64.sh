#!/bin/sh

_install_or_upgrade() {
    download_url=$(curl -L "https://api.github.com/repos/gohugoio/hugo/releases/latest" | jq '.assets[] | select(.name|match("hugo_extended_.*_linux-amd64.deb")) | .browser_download_url' | tr -d \")
    curl -L "${download_url}" -o /tmp/hugo.deb
    sudo apt-get install /tmp/hugo.deb
}

_install() {
    _install_or_upgrade
}

_upgrade() {
    _install_or_upgrade
}

_remove() {
    sudo apt-get remove hugo
}

case $1 in
    _install) _install;;
    _upgrade) _upgrade;;
    _remove) _remove;;
esac