function _install_or_upgrade {
    [xml]$downloads = $(iwr -Uri "https://downloads.yubico.com/")
    $download_path = $($downloads.ListBucketResult.Contents.Key -match "YubiKey-Minidriver-.*-x64.msi" -notmatch ".*.msi_.sha256")

    iwr -Uri "https://downloads.yubico.com/${download_path}" -OutFile "$env:temp\YubiKey-Minidriver-x64.msi"
    msiexec /I "$env:temp\YubiKey-Minidriver-x64.msi"
}

function _install {
    _install_or_upgrade
}

function _upgrade {
    _install_or_upgrade
}

function _remove {
    echo "Please use windows integrated programs parameters or control panel to remove Yubikey Minidriver"
}

switch ($args[0]) {
    "_install" {
        _install
    }
    "_upgrade" {
        _upgrade
    }
    "_remove" {
        _remove
    }
}