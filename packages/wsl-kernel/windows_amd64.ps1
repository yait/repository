function _install_or_upgrade {
    iwr -Uri "https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi" -OutFile "$env:temp\wsl_update_x64.msi"
    msiexec /I "$env:temp\wsl_update_x64.msi"
}

function _install {
    _install_or_upgrade
}

function _upgrade {
    _install_or_upgrade
}

function _remove {
    echo "WSL kernel update cannot be removed"
}

switch ($args[0]) {
    "_install" {
        _install
    }
    "_upgrade" {
        _upgrade
    }
    "_remove" {
        _remove
    }
}