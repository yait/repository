#!/bin/sh

_install_or_upgrade() {
    # shellcheck disable=SC1091
    . /etc/os-release
    
    echo '{ "default-address-pools": [{ "base": "10.10.0.0/16", "size": 24 }] }' > /tmp/default-address-pool.json
    if [ -s /etc/docker/daemon.json ]; then
        jq -s '.[0] * .[1]' /tmp/default-address-pool.json /etc/docker/daemon.json > /tmp/daemon.json
        sudo mv /tmp/daemon.json /etc/docker/daemon.json
    else
        cp /tmp/default-address-pool.json /etc/docker/daemon.json
    fi

    if [ "${ID}" = "debian" ]; then
        sudo update-alternatives --set iptables /usr/sbin/iptables-legacy
        sudo update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
    fi
}

_install() {
    # shellcheck disable=SC1091
    . /etc/os-release

    curl -L "https://download.docker.com/linux/${ID}/gpg" | sudo gpg --dearmor --yes -o /etc/apt/keyrings/docker.gpg
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/${ID} $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list

    sudo apt-get update
    sudo apt-get install docker-ce docker-ce-cli containerd.io
    groups | grep -q "docker" || sudo usermod -aG docker "${USER}"

    _install_or_upgrade
}

_upgrade() {
    sudo apt-get update

    _install_or_upgrade
}

_remove() {
    sudo apt-get remove docker-ce docker-ce-cli containerd.io
}

case $1 in
    _install) _install;;
    _upgrade) _upgrade;;
    _remove) _remove;;
esac