#!/bin/sh

_install_or_upgrade() {
    curl -L "https://github.com/go-swagger/go-swagger/releases/latest/download/swagger_linux_amd64" -o /tmp/swagger
    sudo install -o root -g root -m 0755 /tmp/swagger /usr/local/bin/swagger
}

_install() {
    _install_or_upgrade
}

_upgrade() {
    _install_or_upgrade
}

_remove() {
    sudo rm /usr/local/bin/swagger
}

case $1 in
    _install) _install;;
    _upgrade) _upgrade;;
    _remove) _remove;;
esac