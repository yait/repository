#!/bin/sh

_install() {
    # shellcheck disable=SC1091
    . /etc/os-release
    
    curl -L "https://dl.yarnpkg.com/${ID}/pubkey.gpg" | sudo gpg --dearmor --yes -o /etc/apt/keyrings/yarnkey.gpg
    echo "deb [signed-by=/etc/apt/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/${ID} stable main" | sudo tee /etc/apt/sources.list.d/yarn.list 
    
    sudo apt-get update
    sudo apt-get install yarn
    yarn config set --home enableTelemetry 0
}

_upgrade() {
    sudo apt-get update
    yarn config set --home enableTelemetry 0
}

_remove() {
    sudo apt-get remove yarn
}

case $1 in
    _install) _install;;
    _upgrade) _upgrade;;
    _remove) _remove;;
esac