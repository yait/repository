#!/bin/sh

_install() {
    curl -L "https://deb.nodesource.com/setup_lts.x" | sudo -E bash -
    sudo apt-get update
    sudo apt-get install nodejs
}

_upgrade() {
    sudo apt-get update
}

_remove() {
    sudo apt-get remove nodejs
}

case $1 in
    _install) _install;;
    _upgrade) _upgrade;;
    _remove) _remove;;
esac