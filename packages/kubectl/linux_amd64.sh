#!/bin/bash

_install_or_upgrade() {
    kubectl_version=$(curl -L https://dl.k8s.io/release/stable.txt)
    curl -L "https://dl.k8s.io/release/${kubectl_version}/bin/linux/amd64/kubectl" -o /tmp/kubectl
    curl -L "https://dl.k8s.io/${kubectl_version}/bin/linux/amd64/kubectl.sha256" -o /tmp/kubectl.sha256

    echo "$(cat /tmp/kubectl.sha256) /tmp/kubectl" | sha256sum --check
    sudo install -o root -g root -m 0755 /tmp/kubectl /usr/local/bin/kubectl

    grep -q "alias k=kubectl" ~/.bashrc || echo "alias k=kubectl" >> ~/.bashrc
}

_install() {
    _install_or_upgrade
}

_upgrade() {
    _install_or_upgrade
}

_remove() {
    sudo rm /usr/local/bin/kubectl
    sed -i '#alias k=kubectl#d' ~/.bashrc
}

case $1 in
    _install) _install;;
    _upgrade) _upgrade;;
    _remove) _remove;;
esac