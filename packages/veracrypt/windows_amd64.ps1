function _install_or_upgrade {
    $latest = $(iwr -Uri "https://api.github.com/repos/veracrypt/VeraCrypt/releases/latest" | ConvertFrom-Json)
    $download_url = $($latest.assets.browser_download_url -match "\bVeraCrypt_Setup_x64_.*.msi\b" -notmatch ".*.msi.sig")

    iwr -Uri "${download_url}" -OutFile "$env:temp\VeraCrypt_Setup_x64.msi"
    msiexec /I "$env:temp\VeraCrypt_Setup_x64.msi"
}

function _install {
    _install_or_upgrade
}

function _upgrade {
    _install_or_upgrade
}

function _remove {
    echo "Please use windows integrated programs parameters or control panel to remove VeraCrypt"
}

switch ($args[0]) {
    "_install" {
        _install
    }
    "_upgrade" {
        _upgrade
    }
    "_remove" {
        _remove
    }
}