#!/bin/sh

_install_or_upgrade() {
    curl -L "https://imagemagick.org/archive/ImageMagick.tar.gz" -o /tmp/ImageMagick.tar.gz
    cd "/tmp/$(tar -C /tmp -xvf /tmp/ImageMagick.tar.gz --overwrite)" || exit 2
    ./configure && make
}

_install() {
    _install_or_upgrade
    sudo make install
    sudo ldconfig /usr/local/lib
}

_upgrade() {
    _install_or_upgrade
    sudo make install
}

_remove() {
    _install_or_upgrade
    sudo make uninstall
}

case $1 in
    _install) _install;;
    _upgrade) _upgrade;;
    _remove) _remove;;
esac