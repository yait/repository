function _install_or_upgrade {
    iwr -Uri "https://developers.yubico.com/yubico-piv-tool/Releases/yubico-piv-tool-latest-win64.msi" -OutFile "$env:temp\yubico-piv-tool-latest-win64.msi"
    msiexec /I "$env:temp\yubico-piv-tool-latest-win64.msi"
}

function _install {
    _install_or_upgrade
}

function _upgrade {
    _install_or_upgrade
}

function _remove {
    echo "Please use windows integrated programs parameters or control panel to remove yubico-piv-tool"
}

switch ($args[0]) {
    "_install" {
        _install
    }
    "_upgrade" {
        _upgrade
    }
    "_remove" {
        _remove
    }
}