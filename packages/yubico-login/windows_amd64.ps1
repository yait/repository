function _install_or_upgrade {
    [xml]$downloads = $(iwr -Uri "https://downloads.yubico.com/")
    $download_path = $($downloads.ListBucketResult.Contents.Key -match "Yubico-Login-for-Windows-.*-win64.msi")

    iwr -Uri "https://downloads.yubico.com/${download_path}" -OutFile "$env:temp\Yubico-Login-for-Windows-win64.msi"
    msiexec /I "$env:temp\Yubico-Login-for-Windows-win64.msi"
}

function _install {
    _install_or_upgrade
}

function _upgrade {
    _install_or_upgrade
}

function remove {
    echo "Please use windows integrated programs parameters or control panel to remove Yubico Login"
}

switch ($args[0]) {
    "_install" {
        _install
    }
    "_upgrade" {
        _upgrade
    }
    "_remove" {
        _remove
    }
}