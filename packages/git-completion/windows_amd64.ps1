function _install_or_upgrade {
    iwr -Uri "https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash" -OutFile "$env:userprofile\.git-completion.bash"
    if (!$(sls -Path "$env:userprofile\.bashrc" -Pattern "test -f ~/.git-completion.bash && source ~/.git-completion.bash")) {
        echo "test -f ~/.git-completion.bash && source ~/.git-completion.bash" >> "$env:userprofile\.bashrc"
    }
}

function _install {
    _install_or_upgrade
}

function _upgrade {
    _install_or_upgrade
}

function _remove {
    ri "$env:userprofile\.git-completion.bash"
}

switch ($args[0]) {
    "_install" {
        _install
    }
    "_upgrade" {
        _upgrade
    }
    "_remove" {
        _remove
    }
}