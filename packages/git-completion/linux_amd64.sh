#!/bin/sh

_install_or_upgrade() {
    if [ -f ~/.bashrc ]; then
        curl -L "https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash" -o ~/.git-completion.bash
        grep -q "source ~/.git-completion.bash" ~/.bashrc || echo 'test -f ~/.git-completion.bash && source ~/.git-completion.bash' >> ~/.bashrc
    fi

    if [ -f ~/.zshrc ]; then
        curl -L "https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.zsh" -o ~/.git-completion.zsh
        grep -q "source ~/.git-completion.zsh" ~/.zshrc || echo 'test -f ~/.git-completion.zsh && source ~/.git-completion.zsh' >> ~/.zshrc
    fi

    if [ -f ~/.tcshrc ]; then
        curl -L "https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.tcsh" -o ~/.git-completion.tcsh
        grep -q "source ~/.git-completion.tcsh" ~/.tcshrc || echo 'test -f ~/.git-completion.tcsh && source ~/.git-completion.tcsh' >> ~/.tcshrc
    fi
}

_install() {
    _install_or_upgrade
}

_upgrade() {
    _install_or_upgrade
}

_remove() {
    if [ -f ~/.bashrc ]; then
        rm ~/.git-completion.bash
        sed -i '#test -f ~/.git-completion.bash && source ~/.git-completion.bash#d' ~/.bashrc
    fi

    if [ -f ~/.zshrc ]; then
        rm ~/.git-completion.zsh
        sed -i '#test -f ~/.git-completion.zsh && source ~/.git-completion.zsh#d' ~/.zshrc
    fi

    if [ -f ~/.tcshrc ]; then
        rm ~/.git-completion.tcsh
        sed -i '#test -f ~/.git-completion.tcsh && source ~/.git-completion.tcsh#d' ~/.tcshrc
    fi
}

case $1 in
    _install) _install;;
    _upgrade) _upgrade;;
    _remove) _remove;;
esac