#!/bin/sh

_install_or_upgrade() {
    [ "$(go version)" ] || { echo "go is not installed"; exit 2; }
    curl -L https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b "$(go env GOPATH)/bin"
    grep -q "alias gci=golangci-lint" ~/.bashrc || echo "alias gci=golangci-lint" >> ~/.bashrc
}

_install() {
    _install_or_upgrade
}

_upgrade() {
    _install_or_upgrade
}

_remove() {
    [ "$(go version)" ] || { echo "go is not installed"; exit 2; }
    rm "$(go env GOPATH)/bin/golangci-lint"
    sed -i '#alias gci=golangci-lint#d' ~/.bashrc
}

case $1 in
    _install) _install;;
    _upgrade) _upgrade;;
    _remove) _remove;;
esac