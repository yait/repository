#!/bin/sh

_install() {
    sudo apt-get install postgresql-common
    sudo sh /usr/share/postgresql-common/pgdg/apt.postgresql.org.sh

    sudo apt-get update
    sudo apt-get install postgresql
}

_upgrade() {
    sudo apt-get update
}

_remove() {
    sudo apt-get remove postgresql-common postgresql
}

case $1 in
    _install) _install;;
    _upgrade) _upgrade;;
    _remove) _remove;;
esac