function _install_or_upgrade {
    iwr -Uri "https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh" -OutFile ~/.git-prompt.sh

    if (!$(sls -Path "$env:userprofile\.bashrc" -Pattern "test -f ~/.git-prompt.sh && source ~/.git-prompt.sh")) {
        echo "test -f ~/.git-prompt.sh && source ~/.git-prompt.sh" >> "$env:userprofile\.bashrc"
    }
}

function _install {
    _install_or_upgrade
}

function _upgrade {
    _install_or_upgrade
}

function _remove {
    ri "$env:userprofile\.git-prompt.sh"
}

switch ($args[0]) {
    "_install" {
        _install
    }
    "_upgrade" {
        _upgrade
    }
    "_remove" {
        _remove
    }
}