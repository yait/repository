#!/bin/sh

_install_or_upgrade() {
    curl -L "https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh" -o ~/.git-prompt.sh

    if [ -f ~/.bashrc ] && ! grep -q "source ~/.git-prompt.sh" ~/.bashrc; then
        echo 'test -f ~/.git-prompt.sh && source ~/.git-prompt.sh' >> ~/.bashrc
    fi

    if [ -f ~/.zshrc ] && ! grep -q "source ~/.git-prompt.sh" ~/.zshrc; then
        echo 'test -f ~/.git-prompt.sh && source ~/.git-prompt.sh' >> ~/.zshrc
    fi

    if [ -f ~/.tcshrc ] && ! grep -q "source ~/.git-prompt.sh" ~/.tcshrc; then
        echo 'test -f ~/.git-prompt.sh && source ~/.git-prompt.sh' >> ~/.tcshrc
    fi
}

_install() {
    _install_or_upgrade
}

_upgrade() {
    _install_or_upgrade
}

_remove() {
    rm ~/.git-prompt.sh
    [ -f ~/.bashrc ] && sed -i '#test -f ~/.git-prompt.sh && source ~/.git-prompt.sh#d' ~/.bashrc
    [ -f ~/.zshrc ] && sed -i '#test -f ~/.git-prompt.sh && source ~/.git-prompt.sh#d' ~/.zshrc
    [ -f ~/.tcshrc ] && sed -i '#test -f ~/.git-prompt.sh && source ~/.git-prompt.sh#d' ~/.tcshrc
}

case $1 in
    _install) _install;;
    _upgrade) _upgrade;;
    _remove) _remove;;
esac