#!/bin/sh

_install_or_upgrade() {
    download_url=$(curl -L "https://api.github.com/repos/koalaman/shellcheck/releases/latest" | jq '.assets[] | select(.name|match("shellcheck-.*.linux.x86_64.tar.xz")) | .browser_download_url' | tr -d \")
    curl -L "${download_url}" -o /tmp/shellcheck.tar.xz
    tar -C /tmp -xf /tmp/shellcheck.tar.xz --overwrite
    sudo mv /tmp/shellcheck-* /usr/local/shellcheck
    sudo ln -sf /usr/local/shellcheck/shellcheck /usr/local/bin/shellcheck
}

_install() {
    _install_or_upgrade
}

_upgrade() {
    _install_or_upgrade
}

_remove() {
    sudo rm -rf /usr/local/shellcheck
    sudo find /usr/local/bin -xtype l -delete
}

case $1 in
    _install) _install;;
    _upgrade) _upgrade;;
    _remove) _remove;;
esac