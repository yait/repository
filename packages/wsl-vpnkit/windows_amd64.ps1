function _install_or_upgrade {
    iwr -Uri "https://github.com/sakai135/wsl-vpnkit/releases/latest/download/wsl-vpnkit.tar.gz" -OutFile "$env:temp\wsl-vpnkit.tar.gz"
    wsl --import wsl-vpnkit "$env:userprofile" "$env:temp\wsl-vpnkit.tar.gz"
    echo "Please take appropriate mesures to activate auto startup in your subsystem distributions"
}

function _install {
    _install_or_upgrade
}

function _upgrade {
    wsl --unregister wsl-vpnkit
    _install_or_upgrade
}

function _remove {
    wsl --unregister wsl-vpnkit
    ri "$env:userprofile\wsl-vpnkit" -Recurse
    echo "Please take the appropriate mesures to remove auto startup in your subsystem distributions"
}

switch ($args[0]) {
    "_install" {
        _install
    }
    "_upgrade" {
        _upgrade
    }
    "_remove" {
        _remove
    }
}