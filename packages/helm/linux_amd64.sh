#!/bin/sh

_install_or_upgrade() {
    curl -L "https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3" -o /tmp/get_helm.sh
    chmod 700 /tmp/get_helm.sh
    /tmp/get_helm.sh
}

_install() {
    _install_or_upgrade
}

_upgrade() {
    _install_or_upgrade
}

_remove() {
    sudo rm /usr/local/bin/helm
}

case $1 in
    _install) _install;;
    _upgrade) _upgrade;;
    _remove) _remove;;
esac