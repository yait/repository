## [1.0.0-alpha.2](https://gitlab.com/yait/repository/compare/v1.0.0-alpha.1...v1.0.0-alpha.2) (2023-07-25)


### Chores

* **ci:** removed publish after v4 cicd templates ([beba8ea](https://gitlab.com/yait/repository/commit/beba8ea83aada3f21cddf44155b3ca465f3d6dc0))
* **license:** change to MIT ([5f26d8a](https://gitlab.com/yait/repository/commit/5f26d8a0b29509a8f1047593f2ac627763ab245a))
